# Lightweight (i.e. with a *thin* `/acc`) container to build C++ projects with conan
# ... and some Conanfiles

## Lightweight (i.e. with a *thin* `/acc`) container to build C++ projects with conan

** This depends strongly on [cpp-builder](https://gitlab.cern.ch/pmantion/cpp-builder) **: see [cpp-builder/Readme.md]()
This readme assumes that `cpp-builder` is cloned next to this repo.

### Prepare:

1. This readme assumes that `cpp-builder` is cloned next to this repo.
2. Until codeine with conan support is delivered, the following is required.

    ``
    rsync -a cwe-513-vol956.cern.ch:/user/mvoelkle/codeine/accsoft-codeine-cpp/resources/ ../cpp-builder/cache/acc/local/share/accsoft/accsoft-codeine/0.6.0/
    ``

### Run

1. Run:

    ``
    ./run.sh
    ``
    
# Install Conan + Codeine on a L867 system:

    # Install Conan
    python3 -m venv ./venv
    source ./venv/bin/activate
    pip install conan -i http://acc-py-repo:8081/repository/vr-py-releases/simple --trusted-host acc-py-repo

    # Configure conan
    export CONAN_USER_HOME=/local/conan # On testbed, cs-ccr-ctb01
    # either: (depending on permissions / access to gitlab project)
    conan config install ssh://git@gitlab.cern.ch:7999/pmantion/conan-tools.git --source-folder=cache/conan_config/config/profiles  --target-folder=profiles
    # or:
    conan config install https://gitlab.cern.ch/pmantion/conan-tools.git --source-folder=cache/conan_config/config/profiles  --target-folder=profiles
    # Set cmake executable to use (on L867 machines, or when cmake -> v2, where we want cmake > v3.14)
    sed -i '/^# conan_cmake_program/ s/# conan_cmake_program = cmake/conan_cmake_program = cmake3/' ${CONAN_USER_HOME}/.conan/conan.conf
    conan remote remove conan-center # disable conan-center, as it is pointless on machines without internet access, and will fail
    conan remote add test https://test-artifactory.cern.ch/api/conan/conan-release-local false --insert=0

    # Log into conan remote: login to be had on artifactory: https://test-artifactory/webapp/#/home > Set Me > Up conant-release-local
    conan user -p <TOKEN> --remote test <username>

    # Install codeine from Conan
    conan install accsoft-codeine/0.6.0@ -g virtualrunenv --profile L867-devtoolset-8
    source activate_run.sh

# Codeine -> Conan mapping

## codeine compile:

    # BuildSystem.install()
    mkdir ./build/<target=L867>
    conan install \
        --install-folder ./build/<target=L867> \
        --profile <target=L867> \
        .

    # BuildSystem.build()
    conan build \
        --configure \
        --build \
        --build-folder ./build/<target=L867> \
        .


## codeine test:

    conan build \
        --configure \
        --build \
        --test \
        --build-folder ./build/<target=L867> \


## codeine local-dev-release:

    conan create \
        --env CONAN_RUN_TESTS=<True|False> \
        --env CONAN_CPU_COUNT= \
        --profile <target=L867> \
        .

# Conan

## Status:

### Projects that were exported from `/acc/local` to Conan:
- cmw-util 
- cmw-data 
- cmw-directory-client 
- zmq 
- cmw-rda3 
- cmw-log 
- cmw-rbac 
- cmw-fwk 
- timing 
- ctr 
- timdt-core 
- timdt-lib 
- timdt-lib-cpp 
- timdt-LLlib 
- timdt-metrics 
- timdt-fwk
- cmw-cmx
- cmw-cmx-cpp
- pm-dc-rda3-lib
- accsoft-commons

### Projects that build from source with Conan:
- accsoft-ds-model-cpp
- accsoft-ds-server-cern-cpp
- accsoft-ds-server-cpp
- accsoft-ds-transport-rda3-cpp
- accsoft-ds-fwk (although there is no code in it!)
- fesa-core
- fesa-core-cern
