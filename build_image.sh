#!/usr/bin/bash

BUILD_DIR=${BUILD_DIR:-.}
CONTAINER_CMD=${CONTAINER_CMD:-'podman'}

IMAGE_NAME=accsoft-cpp-conan

${CONTAINER_CMD} build -t ${IMAGE_NAME} -f Containerfile.conan ${BUILD_DIR}
#REMOTE_IMAGE_NAME=gitlab-registry.cern.ch/acc-co/fesa/fwk/fesa-core/${IMAGE_NAME}:handmade
# Tag the built image to be able to push it
#${CONTAINER_CMD} tag ${IMAGE_NAME} ${REMOTE_IMAGE_NAME}
# Push the image to remote
#${CONTAINER_CMD} push ${REMOTE_IMAGE_NAME}
