#!/usr/bin/bash

set -eux

if ! $(podman volume ls | grep -q conan-storage)
then
    podman volume create conan-storage
fi

podman run -ti  \
    -v $PWD/../workspace:/workspace    \
    -v conan-storage:/root/.conan/data            \
    --network=host      \
    accsoft-cpp-conan
#    -v $PWD/../cpp-builder/cache/acc/local:/acc/local                      \
#    -v conan-storage:/home/builder/conan-storage            \
