#!/usr/bin/bash

set -eux

if [[ -f conan_server.pid ]]
then
    kill $(cat conan_server.pid)
fi

export CONAN_STORAGE_PATH="$(podman volume inspect conan-storage | jq -r ".[0].Mountpoint")"

conan_server > conan_server.log &

echo $! > conan_server.pid &

echo "PID of running conan server: $(cat conan_server.pid)"