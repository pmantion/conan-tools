#!/usr/bin/bash

set -eux

ACC_ROOT_PATH="$PWD/../../cpp-builder/cache"

if ! $(podman volume ls | grep -q conan-storage)
then
    podman volume create conan-storage
fi

podman run -ti  \
    -v $PWD/..:/workspace    \
    -v ${ACC_ROOT_PATH}/acc/local:/acc/local                      \
    -v ${ACC_ROOT_PATH}/acc/sys/cdk:/acc/sys/cdk                  \
    -v ${ACC_ROOT_PATH}/acc/sys/Linux:/acc/sys/Linux              \
    -v conan-storage:/root/.conan/data            \
    --network=host      \
    --security-opt seccomp=unconfined \
    accsoft-cpp-conan
#    -v conan-storage:/home/builder/conan-storage            \
