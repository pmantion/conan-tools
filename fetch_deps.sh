#!/usr/bin/bash

set -eux

# Get mvoelkle's unreleased codeine binary
#scp cwe-513-vol956.cern.ch:/user/mvoelkle/codeine/accsoft-codeine-cpp/build/bin/L867/codeine cache/codeine-bin

# Get codeine's unreleased binary (my own locally compiled)
cp ../accsoft-codeine-cpp/build/bin/L867/codeine cache/codeine-bin

# Note that this codeine-launcher script has been modified after being copied from mvoelkle's workspace
# scp cwe-513-vol956.cern.ch:/user/mvoelkle/codeine/bin/codeine cache/codeine-launcher

# Get mvoelkle's conan config
#rsync -a cwe-513-vol956.cern.ch:/user/mvoelkle/codeine/config cache/conan_config